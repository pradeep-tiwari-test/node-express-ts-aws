#!/usr/bin/env sh

if command -v aws; then
  echo "aws-cli already installed, skipping install"
  exit 0
fi

if command -v apk; then
  echo "installing aws-cli with apk..."
  apk update
  apk upgrade
  apk add py3-pip jq
  pip3 install awscli
elif command -v apt-get; then
  echo "installing aws-cli with apt-get..."
  apt-get update -yqqq
  apt-get install -y python3-pip jq
  pip3 install awscli
else
  echo "failed to install aws-cli"
  exit 1
fi
