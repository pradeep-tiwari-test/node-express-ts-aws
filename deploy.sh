set -f
echo "inside deployment"
DEPLOY_SERVERS = $NodeExpressEc2Ip
ALL_SERVERS = (${DEPLOY_SERVERS//,/})
echo "All Servers ${ALL_SERVERS}"
for server in "${ALL_SERVERS[@]}"
do 
    echo "deploying to server ${server}"
    ssh ubuntu@{server} ' bash ' < ./restart.sh
done