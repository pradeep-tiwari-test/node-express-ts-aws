import cookieParser from "cookie-parser";
import express from "express";
const app = express();
app.use(cookieParser());
const port = 8080;

app.get("/", (req:express.Request, res:express.Response) => {
         let value = Math.round(Date.now() / 1000);
         let data = "<html><head></head><body><div>";
         data += `date => ${value}\n`;
         data += "<div><h2>-----cookie data --</h2><div>";
        let cookies = req.headers.cookie ;
        if(cookies)
        {
            
            let eachCookie = cookies.split(';')
            data+= `\n Found ${eachCookie.length} cookies`;
            for(var c of eachCookie)
            {
                let n = c.split('=');
                data+= `\n <h3>${n[0]} = ${n[1]}</h3>`
            }
        }else
        {
            data+="\n found no cookies";
        }
         
        data += "\n</div></div>-----end of cookie data --\n";
        data+= `<br />\nSuccefully started at ${port} at ${Date()}`;
        
        res.cookie("name", "Pradeep");
        let name = 'tscookie';
         
        let noCookie= cookies?.indexOf(name) == -1;
        if(noCookie)
        {
            res.cookie(name, 200,{maxAge:100000});
            
        }else
        {
            let eachCookie = cookies !== undefined ? cookies.split(';') : [];
            for(var c of eachCookie)
            {
                let n = c.split('=');
                if(n[0].trim() == name)
                {
                    let cookieValue = parseInt(n[1].trim());
                    res.cookie(name, cookieValue + 1,{maxAge:1000});
                }else
                {
                    res.cookie(n[0].trim(), n[1].trim());
                }
                
            }
            
        }
        data+="</div></body></html>";
        res.send(data);
});

app.listen(port, ()=> {
    console.log(`Started.. `);
})

